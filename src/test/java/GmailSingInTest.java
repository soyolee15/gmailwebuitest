import com.appsenseca.pageobjects.EmailHomePage;
import com.appsenseca.pageobjects.SignInPage;
import com.appsenseca.utils.WebUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.SystemClock;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Jason on 12/18/2016.
 */
public class GmailSingInTest {
    WebDriver driver;

    @Before
    public void setDriver(){
        String BrowserName = System.getenv("browser");
        if( BrowserName != null && BrowserName.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\chromedriver_win32\\chromedriver.exe");
            driver = new ChromeDriver();
        } else {
            System.setProperty("webdriver.gecko.driver","C:\\Program Files (x86)\\geckodriver.exe");   // Here need to point out geckodriver because of Selenium 3
            driver = new FirefoxDriver();
        }
    }

    @Test
    public void gmailLoginShouldBeSuccessful(){
        // set system environment up
        //System.setProperty("webdriver.gecko.driver","C:\\Users\\Jason\\Desktop\\geckodriver.exe");   // Here need to point out geckodriver because of Selenium 3
        //driver = new FirefoxDriver();
        // open the page
        SignInPage signInPage = WebUtil.goToSignInPage(driver);
        // Fill the username
        signInPage.fillInUsername(driver, "sapjjj15@gmail.com");
        // next
        signInPage.next(driver);
        // fill password
        signInPage.fillInPassword(driver, "Sap123456");
        // click sign in
        EmailHomePage emailHomePage = signInPage.signIn(driver);
        // verify user did sign In
        signInPage.verifySignIn(driver);
        // logout
        signInPage.logOut(driver);
        // verify logout successful
        signInPage.verifyLogout(driver);
    }
    @Test
    public void gmailSendAndReceiveEmailTest(){
        // click sign in
        // open the page
        //System.setProperty("webdriver.gecko.driver","C:\\Users\\Jason\\Desktop\\geckodriver.exe");   // Here need to point out geckodriver because of Selenium 3
        //driver = new FirefoxDriver();
        //driver.get("http://gmail.com");
        SignInPage signInPage = WebUtil.goToSignInPage(driver);
               
        WebDriverWait wait = new WebDriverWait(driver, 30);
        // Fill the username
        signInPage.fillInUsername(driver, "sapjjj15@gmail.com");
        // next
        signInPage.next(driver);
        // fill password
        signInPage.fillInPassword(driver, "Sap123456");
        // click sign in
        signInPage.signIn(driver);
        // verify user did sign In
        signInPage.verifySignIn(driver);
        // click compose
        signInPage.clickCompose(driver);
        // fill in recipient
        signInPage.fillRecipient(driver);
        // fill in subject
        signInPage.fillSubject(driver);
        // fill in email body
        signInPage.fillEmailBody(driver);
        // click send
        signInPage.clickSend(driver);
        // click inbox again
        signInPage.clickInbox(driver);
        // click email
        signInPage.clickEmail(driver);
        // verify the email subject and email body correct
        signInPage.verifyTheSubjectAndBody(driver);
        // logout
        signInPage.logOut(driver);
        // verify logout successful
        signInPage.verifyLogout(driver);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
