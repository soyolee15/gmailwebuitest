package com.appsenseca.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Jason on 12/27/2016.
 */
public class SignInPage {
    public void fillInUsername(WebDriver driver, String s) {
        WebElement usernameTestBox;
        usernameTestBox = driver.findElement(By.id("Email"));
        usernameTestBox.clear();
        usernameTestBox.sendKeys("sapjjj15@gmail.com");
    }

    public void fillInPassword(WebDriver driver, String s) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Passwd")));
        WebElement passwdTestBox;
        passwdTestBox = driver.findElement(By.id("Passwd"));
        passwdTestBox.clear();
        passwdTestBox.sendKeys(s);
    }

    public void next(WebDriver driver) {
        WebElement nextButton;
        nextButton = driver.findElement(By.id("next"));
        nextButton.click();
    }

    public EmailHomePage signIn(WebDriver driver) {
        WebElement signInButton;
        signInButton = driver.findElement(By.id("signIn"));
        signInButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Inbox")));

        return PageFactory.initElements(driver, EmailHomePage.class);
    }

    public void verifySignIn(WebDriver driver) {
        Assert.assertTrue("Inbox should exist",driver.findElements(By.partialLinkText("Inbox")).size() > 0 );
    }

    public void verifyLogout(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signIn")));
        Assert.assertTrue("signIn button should exist",driver.findElements(By.id("signIn")).size() > 0 );
    }

    public void logOut(WebDriver driver) {
        WebElement profileButton = driver.findElement(By.cssSelector("span[class='gb_8a gbii']"));
        profileButton.click();
        WebElement signOutLinkage = driver.findElement(By.id("gb_71"));
        signOutLinkage.click();
    }


    public void clickCompose(WebDriver driver) {
        WebElement ComposeButton =  driver.findElement(By.cssSelector("div[role=button][gh=cm]"));
        ComposeButton.click();
    }

    public void fillRecipient(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("textarea[name=to]")));
        WebElement Recipient = driver.findElement(By.cssSelector("textarea[name=to]"));
        Recipient.clear();
        Recipient.sendKeys("sapjjj15@gmail.com");
    }

    public void fillSubject(WebDriver driver) {
        WebElement subjectTestArea = driver.findElement(By.cssSelector("input[name=subjectbox]"));
        final String subjectText = "Gmail send Email Test";
        subjectTestArea.clear();
        subjectTestArea.sendKeys(subjectText);
    }

    public void fillEmailBody(WebDriver driver) {
        WebElement bodyTestArea = driver.findElement(By.cssSelector("div[aria-label='Message Body']"));
        final String bodyText = "Hello Tester! Good Morning!";
        bodyTestArea.clear();
        bodyTestArea.click();
        bodyTestArea.sendKeys(bodyText);
    }

    public void clickSend(WebDriver driver) {
        WebElement sendButton = driver.findElement(By.cssSelector("div[aria-label*=\"Send\"]"));
        sendButton.click();
    }

    public void clickInbox(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Inbox (1)")));
        WebElement inboxLinkage = driver.findElement(By.linkText("Inbox (1)"));
        inboxLinkage.click();
    }

    public void clickEmail(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class=y6] span[id] b")));
        WebElement newEmail = driver.findElement(By.cssSelector("div[class=y6] span[id] b"));
        newEmail.click();
    }

    public void verifyTheSubjectAndBody(WebDriver driver) {
        final String subjectText = "Gmail send Email Test";
        final String bodyText = "Hello Tester! Good Morning!";
        WebElement subjectArea = driver.findElement(By.cssSelector("h2[class='hP']"));
        Assert.assertEquals("Email subject should be the same", subjectText, subjectArea.getText());
        WebElement bodyArea = driver.findElement(By.cssSelector("div[class='nH aHU'] div[dir='ltr']"));
        Assert.assertEquals("Email body should be the same", bodyText, bodyArea.getText());
    }
}
