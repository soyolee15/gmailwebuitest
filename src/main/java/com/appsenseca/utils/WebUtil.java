package com.appsenseca.utils;

import com.appsenseca.pageobjects.SignInPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Jason on 12/27/2016.
 */
public class WebUtil {
    public static SignInPage goToSignInPage(WebDriver driver) {
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Jason\\Desktop\\geckodriver.exe");   // Here need to point out geckodriver because of Selenium 3
        driver.get("http://gmail.com");
        return PageFactory.initElements(driver, SignInPage.class);
    }
}
